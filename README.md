Raja: Réservation Aisée de Jeux Amicaux
=======================================

An online booking framework for convention games and other events.


Features
--------

* No account creation necessary for users
* Compatible with text-only web browsers
* Can be used with Javascrit disabled


Requirements
------------

* Python >= 3.6


Getting Started
---------------

* Create a Python virtual environment.
```
python3 -m venv env
```

* Upgrade packaging tools.
```
env/*/pip install --upgrade pip setuptools
```

* Install the project in editable mode with its testing requirements.
```
env/*/pip install -e ".[testing]"
```

* Upgrade to the latest database revision.
```
env/*/alembic -c development.ini upgrade head
```

* Load default data into the database using a script.
```
env/*/initialize_raja_db development.ini
```

* Run the project's tests.
```
env/*/pytest
```

* Run the project.

```
env/*/pserve development.ini
```
