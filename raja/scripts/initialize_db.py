import argparse
import sys

from pyramid.paster import bootstrap, setup_logging

from .. import models


def setup_models(dbsession):
    """
    Add or update models / fixtures in the database.

    """
    print("Config")
    dbsession.add(models.config.Config(variable="phase", value="0"))


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'config_uri',
        help='Configuration file, e.g., development.ini',
    )
    return parser.parse_args(argv[1:])


def main(argv=sys.argv):
    args = parse_args(argv)
    setup_logging(args.config_uri)
    env = bootstrap(args.config_uri)

    with env['request'].tm:
        dbsession = env['request'].dbsession
        setup_models(dbsession)
