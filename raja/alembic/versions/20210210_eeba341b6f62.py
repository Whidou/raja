"""init

Revision ID: eeba341b6f62
Revises:
Create Date: 2021-02-10 15:39:12.699952

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'eeba341b6f62'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # Config
    config_columns = (
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('variable', sa.String(length=31), nullable=False),
        sa.Column('value', sa.UnicodeText(), nullable=True))
    config_constraint = sa.PrimaryKeyConstraint('id', name=op.f('pk_config'))
    op.create_table('config', *config_columns, config_constraint)

    op.create_index('idx_config_variable', 'config', ['variable'],
                    unique=True, mysql_length=31)

    # Events
    event_columns = (
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('title', sa.UnicodeText(), nullable=False),
        sa.Column('topic', sa.UnicodeText(), nullable=True),
        sa.Column('details', sa.UnicodeText(), nullable=True),
        sa.Column('private', sa.UnicodeText(), nullable=True),
        sa.Column('seats', sa.Integer(), nullable=True),
        sa.Column('start', sa.DateTime(), nullable=False),
        sa.Column('end', sa.DateTime(), nullable=False),
        sa.Column('owner', sa.UnicodeText(), nullable=False),
        sa.Column('email', sa.Text(), nullable=True),
        sa.Column('token', sa.Text(), nullable=False))
    event_constraint = sa.PrimaryKeyConstraint('id', name=op.f('pk_events'))
    op.create_table('events', *event_columns, event_constraint)

    op.create_index('idx_event_title', 'events', ['title'],
                    unique=True, mysql_length=255)

    # Bookings
    booking_columns = (
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('event', sa.Integer(), nullable=False),
        sa.Column('name', sa.UnicodeText(), nullable=False),
        sa.Column('email', sa.Text(), nullable=True),
        sa.Column('seats', sa.Integer(), nullable=False),
        sa.Column('token', sa.Text(), nullable=False),
        sa.Column('note', sa.UnicodeText(), nullable=True))
    booking_constraints = (
        sa.ForeignKeyConstraint(['event'], ['events.id'],
                                name=op.f('fk_bookings_event_events')),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_bookings')))
    op.create_table('bookings', *booking_columns, *booking_constraints)

    op.create_index('idx_booking_event', 'bookings', ['event'], unique=False)


def downgrade():
    op.drop_index('idx_booking_event', table_name='bookings')
    op.drop_table('bookings')
    op.drop_index('idx_event_title', table_name='events')
    op.drop_table('events')
    op.drop_index('idx_config_variable', table_name='config')
    op.drop_table('config')
