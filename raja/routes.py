def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_static_view("static_deform", "deform:static")
    config.add_route('home', '/')
    config.add_route('events', '/parties/')
    config.add_route('manage', '/gestion/{token:[a-zA-Z0-9_=+-]*}')
    config.add_route('details', '/details/{id:[0-9]+}')
    config.add_route('edit', '/partie/{token:[a-zA-Z0-9_=+-]*}')
    config.add_route('book', '/reservation/{id:[0-9]+}/{token:[a-zA-Z0-9_=+-]*}')
