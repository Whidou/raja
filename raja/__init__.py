from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory

from .models import Config


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    session_factory = SignedCookieSessionFactory("temporary test secret")

    with Configurator(settings=settings,
                      session_factory=session_factory) as config:
        config.include('.models')
        config.include('pyramid_jinja2')
        config.include('.routes')
        config.scan()
        app = config.make_wsgi_app()
        session = config.registry['dbsession_factory']()

    # Display management token
    management_token = session.query(Config).\
        filter_by(variable="management_token").first()
    session.close()
    if management_token:
        print("Management token:", management_token.value)

    return app
