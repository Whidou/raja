from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound, HTTPNotFound

from secrets import token_urlsafe

from ..models import Event, Booking, Config


@view_config(route_name='manage', renderer='../templates/manage.jinja2')
def manage_view(request):
    session = request.dbsession
    user_token = request.matchdict['token']

    # Get management token or create anew one
    management_token = session.query(Config).\
        filter_by(variable="management_token").first()
    if not management_token:
        management_token = token_urlsafe(6)
        session.add(Config(variable="management_token",
                           value=management_token))
        return HTTPFound(location="/manage/{}".format(management_token))

    # Check if management token is valid
    if user_token != management_token.value:
        raise HTTPNotFound()

    # Get database entries
    events = session.query(Event).all()
    bookings = session.query(Booking).all()
    for event in events:
        event.bookings = frozenset(b for b in bookings if b.event == event.id)
        event.booked = sum(b.seats for b in event.bookings)

    return {"events": events}
