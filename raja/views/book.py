from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound

from colander import (
    Email,
    Function,
    Integer,
    Length,
    Range,
    Regex,
    SchemaNode,
    String,
)

import deform

from secrets import token_urlsafe

from sqlalchemy import func

from ..models import Booking, Event


class BookSchema(deform.schema.CSRFSchema):

    name = SchemaNode(String(), title="Prénom ou pseudonyme",
                      description="Personne effectuant la réservation")

    email = SchemaNode(String(), title="Courriel",
                       validator=Email(),
                       missing="",
                       description="""\
Adresse de courriel, optionnelle, utilisée pour avertir de tout changement ou \
annulation concernant la partie, ne sera pas transmise à l'animateur.""")

    seats = SchemaNode(Integer(), title="Places",
                       description="Nombre de places à réserver",
                       default=1,
                       widget=deform.widget.TextInputWidget(attributes={
                        "type": "number",
                        "min": "1"}))

    note = SchemaNode(String(), title="Note",
                      validator=Length(max=1000),
                      missing="",
                      widget=deform.widget.RichTextWidget(rows=10, cols=60),
                      description="""\
Note à l'attention de l'animateur, optionnelle, par exemple pour demander un
aménagement particulier.""")

    token = SchemaNode(String(), widget=deform.widget.HiddenWidget(),
                       validator=Regex(r"^[a-zA-Z0-9_=+-]+$"),
                       default="-")

    id = SchemaNode(Integer(), widget=deform.widget.HiddenWidget(), default=-1)


@view_defaults(route_name='book',
               renderer='../templates/book.jinja2')
class BookView:
    def __init__(self, request):
        self.request = request
        self.session = self.request.dbsession
        self.values = {}
        self.event_id = request.matchdict['id']
        self.token = request.matchdict['token']
        self.remaining = self.get_remaining_seats()

        buttons = [deform.Button(name='send', title="Réserver")]
        appstruct = {}
        if self.token:
            # Retrieve booking from database
            query = self.session.query(Booking).filter_by(event=self.event_id,
                                                          token=self.token)
            booking = query.first()
            if booking:
                # Retrieve private note for participants
                private_note = self.session.query(Event.private).\
                    filter_by(id=self.event_id).first()
                if private_note:
                    self.values["note"] = private_note[0]

                appstruct = booking.__dict__
                self.values["edit"] = True
                buttons = [deform.Button(name='send', title="Modifier"),
                           deform.Button(name='delete', title="Supprimer",
                                         css_class="btn-danger")]

        schema = BookSchema().bind(request=self.request)
        schema["seats"].validator = Function(self.seat_validator)
        schema["seats"].widget.attributes["max"] = self.remaining
        self.form = deform.Form(schema, buttons=buttons)
        self.values["form"] = self.form.render(appstruct)

    @view_config(request_method='GET')
    def handle_get_request(self):
        return self.values

    @view_config(request_method='POST')
    def handle_post_request(self):
        try:
            appstruct = self.form.validate(self.request.POST.items())
        except deform.ValidationFailure as e:
            self.values["form"] = e.render()
            return self.values

        delete = self.request.POST.get("delete", False)
        self.store_booking(appstruct, delete)

        url = self.request.route_url('book', id=self.event_id,
                                     token=appstruct["token"])
        return HTTPFound(location=url)

    def store_booking(self, values, delete=False):
        values.pop("csrf_token")
        values["event"] = self.event_id

        # New booking
        if values["id"] < 0:
            values.pop("id")
            values["token"] = token_urlsafe(6)
            self.session.add(Booking(**values))
            return

        # Existing booking, check that the token is correct
        query = self.session.query(Booking).filter_by(id=values["id"],
                                                      event=values["event"],
                                                      token=values["token"])
        booking = query.first()
        if not booking:
            return

        if delete:
            # Booking deletion
            self.session.delete(booking)
        else:
            # Booking update
            self.session.merge(Booking(**values))

    def get_remaining_seats(self):
        total = self.session.query(Event.seats).\
            filter_by(id=self.event_id).first()
        if not total:
            return 0

        booked = self.session.query(func.sum(Booking.seats)).\
            filter_by(event=self.event_id).group_by(Booking.event).first()
        if not booked:
            booked = (0, )

        return total[0] - booked[0]

    def seat_validator(self, value):
        if value < 1:
            return "Une place au moins doit être réservée"

        if int(value) > self.remaining:
            return "Pas assez de places disponibles"

        return True
