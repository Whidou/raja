from pyramid.view import view_config

from ..models import Event, Booking


@view_config(route_name='details', renderer='../templates/details.jinja2')
def details_view(request):
    event_id = request.matchdict['id']
    session = request.dbsession

    event = session.query(Event).filter_by(id=event_id).first()
    seat_results = session.query(Booking.seats).filter_by(event=event_id).all()
    seats = sum(x[0] for x in seat_results)

    return {"event": event, "seats": seats}
