from pyramid.view import view_config
from sqlalchemy import func

from ..models import Event, Booking


@view_config(route_name='events',
             renderer='../templates/events.jinja2')
def presentation_view(request):
    session = request.dbsession
    events = session.query(Event).all()

    bookings = session.query(Booking.event, func.sum(Booking.seats)).\
        group_by(Booking.event).all()
    bookings = dict(bookings)
    for event in events:
        event.booked = bookings.get(event.id, 0)

    return {'events': events}
