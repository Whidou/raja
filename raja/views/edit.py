from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound

from colander import (
    DateTime,
    Email,
    Integer,
    Length,
    Range,
    Regex,
    SchemaNode,
    String,
)

import deform

from secrets import token_urlsafe

from ..models import Event, Booking


class EditSchema(deform.schema.CSRFSchema):

    title = SchemaNode(String(), title="Nom",
                       description="Nom de la partie")

    topic = SchemaNode(String(), title="Jeu",
                       description="Activité ou jeu proposé")

    start = SchemaNode(DateTime(), title="Début",
                       description="Date et heure de début")

    end = SchemaNode(DateTime(), title="Fin",
                     description="Date et heure de fin")

    seats = SchemaNode(Integer(), title="Places",
                       validator=Range(0, None),
                       description="Nombre de places",
                       widget=deform.widget.TextInputWidget(attributes={
                        "type": "number",
                        "min": "0"}))

    details = SchemaNode(String(), title="Détails",
                         validator=Length(max=1000),
                         widget=deform.widget.RichTextWidget(rows=10, cols=60),
                         description="Description de la partie")

    private = SchemaNode(String(), title="Informations pour les inscrits",
                         validator=Length(max=1000),
                         missing="",
                         widget=deform.widget.RichTextWidget(rows=10, cols=60),
                         description="""\
Détails réservés aux inscrits, par exemple un lien vers la table virtuelle \
utilisée ou des instructions de création de personnage.""")

    owner = SchemaNode(String(), title="Animateur",
                       description="Prénom ou pseudonyme de l'animateur")

    email = SchemaNode(String(), title="Courriel",
                       validator=Email(),
                       missing="",
                       description="""\
Adresse de courriel de l'animateur, optionnelle, utilisée pour avertir de \
nouvelles inscriptions ou désistements.""")

    token = SchemaNode(String(), widget=deform.widget.HiddenWidget(),
                       validator=Regex(r"^[a-zA-Z0-9_=+-]+$"),
                       default="-")

    id = SchemaNode(Integer(), widget=deform.widget.HiddenWidget(), default=-1)


@view_defaults(route_name='edit',
               renderer='../templates/edit.jinja2')
class EditView:
    def __init__(self, request):
        self.request = request
        self.session = self.request.dbsession
        self.values = {}
        self.token = request.matchdict['token']

        buttons = [deform.Button(name='send', title="Créer")]
        appstruct = {}
        if self.token:
            # Retrieve event from database
            event = self.session.query(Event).filter_by(token=self.token).\
                first()

            if event:
                # Retrieve bookings
                bookings = self.session.query(Booking).\
                    filter_by(event=event.id).all()
                self.values["bookings"] = bookings

                # Pre-fill form
                appstruct = event.__dict__
                self.values["edit"] = True
                buttons = [deform.Button(name='send', title="Modifier"),
                           deform.Button(name='delete', title="Supprimer",
                                         css_class="btn-danger")]

        # Create form
        schema = EditSchema().bind(request=self.request)
        self.form = deform.Form(schema, buttons=buttons)
        self.values["form"] = self.form.render(appstruct)

    @view_config(request_method='GET')
    def handle_get_request(self):
        return self.values

    @view_config(request_method='POST')
    def handle_post_request(self):
        try:
            appstruct = self.form.validate(self.request.POST.items())
        except deform.ValidationFailure as e:
            self.values["form"] = e.render()
            return self.values

        delete = self.request.POST.get("delete", False)
        self.store_event(appstruct, delete)

        url = self.request.route_url('edit', token=appstruct["token"])
        return HTTPFound(location=url)

    def store_event(self, values, delete=False):
        values.pop("csrf_token")

        # New event
        if values["id"] < 0:
            values.pop("id")
            values["token"] = token_urlsafe(6)
            self.session.add(Event(**values))
            return

        # Existing event, check that the token is correct
        query = self.session.query(Event).filter_by(id=values["id"],
                                                    token=values["token"])
        event = query.first()
        if not event:
            return

        if delete:
            # Event deletion
            self.session.delete(event)
        else:
            # Event update
            self.session.merge(Event(**values))
