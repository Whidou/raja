from sqlalchemy import (
    Column,
    DateTime,
    Index,
    Integer,
    Text,
    UnicodeText,
)

from .meta import Base


class Event(Base):
    __tablename__ = 'events'
    id = Column(Integer, primary_key=True)
    title = Column(UnicodeText, nullable=False)
    topic = Column(UnicodeText)
    details = Column(UnicodeText)
    private = Column(UnicodeText)
    seats = Column(Integer)
    start = Column(DateTime, nullable=False)
    end = Column(DateTime, nullable=False)
    owner = Column(UnicodeText, nullable=False)
    email = Column(Text)
    token = Column(Text, nullable=False)


Index('idx_event_title', Event.title, unique=True, mysql_length=255)
