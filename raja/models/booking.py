from sqlalchemy import (
    Column,
    ForeignKey,
    Index,
    Integer,
    Text,
    UnicodeText,
)

from .meta import Base


class Booking(Base):
    __tablename__ = 'bookings'
    id = Column(Integer, primary_key=True)
    event = Column(Integer, ForeignKey("events.id"), nullable=False)
    name = Column(UnicodeText, nullable=False)
    email = Column(Text)
    seats = Column(Integer, nullable=False)
    token = Column(Text, nullable=False)
    note = Column(UnicodeText)


Index('idx_booking_event', Booking.event, unique=False)
